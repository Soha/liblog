# Log Library

## License

### The MIT License (MIT)

###### Copyright (c) 2016 Arthur Mahéo <maheo[dot]arthur[at]gmail.com>

See the [license file](LICENSE).

## What Is It?

Logging namespace. Actually more of a verbose actor: initialized with
a threshold, then called upon with a set of methods based on the
importance of the message. Useful when you want to print debug
information while coding, but silence them once running in
production. This is possible because the verbositiy can be set at
runtime.

Items called with this class should all extend the `<<` operator.

## Usage

 - Import the header `"Log.h"` then use `Log::<function>()` to call a
   given level of logging.

 - By default, Log level is `Log::USER`; it can be adjusted at runtime
   by:
    + setting the level with `Log::setLevel()`;
    + increasing/decreasing the level with
      `Log::incLevel()`/`Log::decLevel()`.

 - For complex formatting, or specific output, it is possible to check
   the current level of logging using `Log::test()` or `Log::is()`.

 - By default, the output stream is `std::cout`, but it is possible to
   change this by using `Log::setStream()`.

 - *Note:* `Log::error()` not only always outputs someting but also
           exits the program with `EXIT_FAILURE`.

## Examples

Considering user level logging.
```c++
Log::setLevel(Log::USER);
```

### Different log levels

```c++
Log::log("I am log...\n");
Log::note("I am note~\n");
Log::user("I am user.\n");
Log::warning("I am warning!\n");
Log::error("I am ERROR!\n");
```
Output:
```
I am user.
I am warning!
I am ERROR!
```

### Level checking

```c++
if (Log::test(Log::NOTE)) {
  cout << "Hidden here." << endl;
}

if (Log::test(Log::WARNING)) {
  cout << "Visible there." << endl;
}

if (Log::is(Log::USER)) {
  cout << "It is user.";
} else {
  cout << "It is not user." << endl;
}
```

Output:
```
Visible there.
It is user.
```

### String formatting

```c++
string format = "format";
int    num    = 42;
float  golden = 1.61803398875;

Log::user("We can also %s strings with all sorts of things:\n"
          "\t%d (%p)\n"
          "\t%f (%.2f)\n", format.c_str(), num, &num, golden, golden);
```

Example output:
```
We can also format strings with all sorts of things:
	42 (0x7fff37ef84cc)
	1.618034 (1.62)
```
