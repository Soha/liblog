/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Arthur Mahéo <maheo[dot]arthur[at]gmail.com>
 *
 * Logging namespace. Actually more of a verbose actor: initialized
 * with a threshold, then called upon with a set of methods based on
 * the importance of the message. Useful when you want to print debug
 * information while coding, but silence them once running in
 * production. This is possible because the verbositiy can be set at
 * runtiem.
 *
 * Items called with this class should all extend the `<<` operator.
 *
 * Usage:
 *
 *  - Import the header then use Log::<function>() to call a given
 *    level of logging.
 *
 *  - By  default, Log level is Log::USER; it can be adjusted at runtime by:
 *     + setting the level with Log::setLevel();
 *     + increasing/decreasing the level with Log::incLevel()/Log::decLevel().
 *
 *  - For complex formatting, or specific output, it is possible to
 *    check the current level of logging using Log::test() or Log::is().
 *
 *  - By default, the output stream is std::cout, but it is possible
 *    to change this by using Log::setStream().
 *
 *  - Note: Log::error() not only always outputs someting but also
 *          exits the program with EXIT_FAILURE.
 *
 * Examples:
 *
 *   Log::setLevel(Log::USER);
 *
 *   // Different log levels
 *   Log::log("I am log...");       // <blank>
 *   Log::note("I am note~");       // <blank>
 *   Log::user("I am user.");       // I am user.
 *   Log::warning("I am warning!"); // I am warning!
 *   Log::error("I am ERROR!");     // "I am ERROR!" *and* exits the program
 *
 *   // Playing with level checking
 *   if (Log::test(Log::NOTE)) {
 *     cout << "Hidden here.";      // <blank>
 *   }
 *
 *   if (Log::test(Log::WARNING)) {
 *     cout << "Visible there.";    // Visible there.
 *   }
 *
 *   if (Log::is(Log::USER)) {
 *     cout << "It is user.";       // It is user.
 *   } else {
 *     cout << "It is not user.";
 *   }
 *
 *   // String formatting
 *   string format = "format";
 *   int    num    = 42;
 *   float  golden = 1.61803398875;
 *
 *   Log::user("We can also %s strings with all sorts of things:\n"
 *             "\t%d (%p)\n"
 *             "\t%f (%.2f)\n", format.c_str(), num, &num, golden, golden);
 *
 *   // We can also format strings with all sorts of things:
 *   //   42 (0x7fff37ef84cc)
 *   //   1.618034 (1.62)
 */
#ifndef LOG_HPP
#define	LOG_HPP

#include <iostream>
#include <cstdlib>
#include <stdio.h>

const int LOG_BUFFER = 99;

namespace Log {

  typedef enum {
    NONE,
    ERROR,
    WARNING,
    USER,
    NOTICE,
    DEBUG
  } t_level;

  extern std::ostream *_stream;
  extern t_level _level;

  template <class T> void _log(const T&, const t_level&);
  template <class T> void _log(const std::string&, const T&, const t_level&);
  template <typename... Args> void _log(const std::string&, const t_level&, const Args&...);

  void setStream(std::ostream&);
  void setLevel(const int&);
  void incLevel();
  void decLevel();
  bool test(const t_level&);
  bool is(const t_level&);

  // Display functions
  template <class T> void log(const T&);
  template <class T> void note(const T&);
  template <class T> void user(const T&);
  template <class T> void warning(const T&);
  template <class T> void error(const T&);
  // Formatted display
  template <class T> void log(const std::string, const T&);
  template <class T> void note(const std::string, const T&);
  template <class T> void user(const std::string, const T&);
  template <class T> void warning(const std::string, const T&);
  template <class T> void error(const std::string, const T&);
  // Variadic formatted display
  template <typename... Args> void log(const std::string&, const Args&...);
  template <typename... Args> void note(const std::string&, const Args&...);
  template <typename... Args> void user(const std::string&, const Args&...);
  template <typename... Args> void warning(const std::string&, const Args&...);
  template <typename... Args> void error(const std::string&, const Args&...);
}

// Templated functions have to be defined in the header file...

/**
 * Display `item` only if log level is high enough. Also force stream flush to
 * have "real-time" data.
 *
 * @param item
 * @param threshold
 */
template <class T> void Log::_log(const T &item, const t_level &threshold)
{
  if (test(threshold)) {
    *_stream << item;
    _stream->flush();
  }
}
// Format before display

template <class T> void Log::_log(const std::string &str,
                                  const T &item,
                                  const t_level &threshold)
{
  char *log = (char*) malloc((str.length() + LOG_BUFFER) * sizeof (char));

  sprintf(log, str.c_str(), item);

  Log::_log(log, threshold);

  free(log);
}

template <typename... Args> void Log::_log(const std::string &format,
                                           const t_level &threshold,
                                           const Args&... data)
{
  if (test(threshold)) {
    char *log = (char*) malloc((format.length() + LOG_BUFFER) * sizeof (char));

    sprintf(log, format.c_str(), data...);

    Log::_log(log, threshold);

    free(log);
  }
}

/**
 * Debug info. Unnecessary when using final program.
 *
 * @param item
 */
template <class T> void Log::log(const T &item)
{
  _log(item, DEBUG);
}

template <class T> void Log::log(const std::string str, const T &item)
{
  _log(str, item, DEBUG);
}

template <typename... Args> void Log::log(const std::string &format,
                                          const Args&... data)
{
  _log(format, DEBUG, data...);
}

/**
 * Notices, show the advancement of the program.
 *
 * @param item
 */
template <class T> void Log::note(const T &item)
{
  _log(item, NOTICE);
}

template <class T> void Log::note(const std::string str, const T &item)
{
  _log(str, item, NOTICE);
}

template <typename... Args> void Log::note(const std::string &format,
                                           const Args&... data)
{
  _log(format, NOTICE, data...);
}

/**
 * User display, show basic data on the program.
 *
 * @param item
 */
template <class T> void Log::user(const T &item)
{
  _log(item, USER);
}

template <class T> void Log::user(const std::string str, const T &item) {
  _log(str, item, USER);
}

template <typename... Args> void Log::user(const std::string &format,
                                           const Args&... data)
{
  _log(format, USER, data...);
}

/**
 * Uncorrect but non fatal behaviour.
 *
 * @param item
 */
template <class T> void Log::warning(const T &item)
{
  _log(item, WARNING);
}

template <class T> void Log::warning(const std::string str, const T &item)
{
  _log(str, item, WARNING);
}

template <typename... Args> void Log::warning(const std::string &format,
                                              const Args&... data)
{
  _log(format, WARNING, data...);
}

/**
 * Fatal error, print message and exits.
 *
 * @param item
 */
template <class T> void Log::error(const T &item)
{
  _log(item, ERROR);
  exit(EXIT_FAILURE);
}

template <class T> void Log::error(const std::string str, const T &item)
{
  _log(str, item, ERROR);
  exit(EXIT_FAILURE);
}

template <typename... Args> void Log::error(const std::string &format,
                                            const Args&... data)
{
  _log(format, ERROR, data...);
  exit(EXIT_FAILURE);
}

#endif	/* LOG_HPP */
