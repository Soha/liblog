/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Arthur Mahéo <maheo[dot]arthur[at]gmail.com>
 */

#include "Log.h"

// Default values
Log::t_level Log::_level = Log::USER;
std::ostream *Log::_stream = &std::cout;

// Setters

void Log::setStream(std::ostream &os)
{
  Log::_stream = &os;
}

void Log::setLevel(const int &l)
{
  switch (l) {
    case DEBUG:
      Log::_level = DEBUG;
      break;

    case NOTICE:
      Log::_level = NOTICE;
      break;

    case USER:
      Log::_level = USER;
      break;

    case WARNING:
      Log::_level = WARNING;
      break;

    case ERROR:
      Log::_level = ERROR;
      break;

    case NONE:
      Log::_level = NONE;
      break;

    default:
      Log::error("Unknown type [%d] in Log::setLevel().\n", l);
  }
}

/**
 * Decrement the log level.
 */
void Log::decLevel()
{
  switch (_level) {
    case DEBUG:
      _level = NOTICE;
      break;

    case NOTICE:
      _level = USER;
      break;

    case USER:
      _level = WARNING;
      break;

    case WARNING:
      _level = ERROR;
      break;

    case ERROR:
      _level = NONE;
      break;

    case NONE:
      // Nothing
      break;
  }
}

/**
 * Increment the log level.
 */
void Log::incLevel()
{
  switch (_level) {
    case DEBUG:
      // Nothing
      break;

    case NOTICE:
      _level = DEBUG;
      break;

    case USER:
      _level = NOTICE;
      break;

    case WARNING:
      _level = USER;
      break;

    case ERROR:
      _level = WARNING;
      break;

    case NONE:
      _level = ERROR;
      break;
  }
}

/**
 * Test against current log level.
 *
 * @param level
 *
 * @return test is under -- authorized -- current log level
 */
bool Log::test(const t_level &level)
{
  return Log::_level >= level;
}

bool Log::is(const t_level &level)
{
  return Log::_level == level;
}
